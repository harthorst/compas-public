# 3rd party libraries used
## FFmpeg

This app uses FFmpeg for decodig and resampling instrument samples. This 
documentation describes how you can change the FFmpeg binaries in the app.
You'll need a Mac or Linux system for this.

**This howto requires profound knowledge about android build and platform tools**   

### Compiling FFmpeg

#### Download sources and build tools
This article desribes what you'll need: https://medium.com/@karthikcodes1999/cross-compiling-ffmpeg-4-0-for-android-b988326f16f2

#### download build scripts 
Fortunately there are some helper scripts already prepared. Thanks to Don Turner.

https://gist.github.com/dturner

You'll need only the `.sh` scripts

##### Compile FFmpeg
1) Open the downloaded `build.sh` and set `HOST_OS_ARCH` to your build system
2) execute `build.sh` (maybe you have to set some environment variables like NDK location, etc.)

You should find a bunch of `.so` files in the output folder now!!!

### Getting and disassembling the APK
1) install the app
2) use Android Studios device explorer or adb to copy the apk to you local file system
3) unzip the apk

### Replacing FFmpeg
1) replace the `.so` files in the extracted apk with the newly compiled ones FFmpeg files. Be sure you preserve the correct ABI folder (x86, armeabi-v7a, ...)
2) remove signature from apk by deleting anything under META-INF
3) reassemble the app by just zipping it 

### Install the app
You can use `adb install` for installing the app 