# Another Flamenco Comp�s App

Welcome to the public repository of "Another Flamenco Comp�s App". Here you can add bug reports and feature requests.

## How to create a bug report or feature request

1. Click "Issues" on the left navigation bar
2. Check that the issue has not already been reported 
3. Click the "Create Issue" button
4. Fill in all mandatory information
5. Don't forget to set the correct issue kind.





